// @flow

import React from 'react';
import './App.css';
import Header from './components/shared/Header/Header';
import Sidebar from './components/shared/Sidebar/Sidebar';
import Main from './components/shared/Main/Main';

// class App extends Component {
//   render() {
//     return (
//       <div className="App">
//         <header className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <h1 className="App-title">Welcome to React</h1>
//         </header>
//         <p className="App-intro">
//           To get started, edit <code>src/App.js</code> and save to reload.
//         </p>
//       </div>
//     );
//   }
// }
//
// export default App;

import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom'

const App = () => (
    <Router>
        <div>
            <Header />
            <div className="container pt-card pt-elevation-0">
                <Sidebar />
                <Main />
            </div>
        </div>
    </Router>
);

export default App