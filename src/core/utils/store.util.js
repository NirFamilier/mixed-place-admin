// @flow

export class StoreUtil {
    appStorage = window.localStorage;

    getStorage(name) {
        const ca = this.appStorage.getItem(name).split(';');
        const caLen = ca.length;
        const storageKeyName = name + "=";
        let c;

        for (let i = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s\+/g, "").trim();
            if (c.indexOf(storageKeyName) === 0) {
                return c.substring(storageKeyName.length, c.length);
            }
        }
        return '';
    }

    deleteStorage(name) {
        this.appStorage.removeItem(name);
    }

    setStorageExplicitExp(name, value, expires, path = "") {
        let d = new Date(expires);
        this.appStorage.setItem(name, `${name}=${value}; expires=${d.toUTCString()};path=/`);
    }

    setStorage(name, value, expireSec, path = "") {
        const d = new Date();
        d.setTime(d.getTime() + (expireSec  * 1000));
        const expires = d.toUTCString();
        this.appStorage.setItem(name, `${name}=${value}; expires=${expires};path=/`);
    }
}


const storeUtil = new StoreUtil();
Object.freeze(storeUtil);

export default storeUtil;