// @flow

import {ErrorTypes} from "../models/enums";

export class ApiUtil {

    // headers = new Headers({
    //     // "Content-Type": "text/plain",
    //     // "Content-Length": content.length.toString(),
    //     // "X-Custom-Header": "ProcessThisImmediately",
    // });

    get = (url, options) => {
        const fetchData = {
            method: 'GET',
            body: options,
            // mode: 'cors',
            // cache: 'default',
            // headers: this.headers
        };

        return this.fetchCall(url, fetchData)
    };

    post = (url, options) => {
        const fetchData = {
            method: 'POST',
            body: JSON.stringify(options),
            headers: this.headers
        };

        return this.fetchCall(url, fetchData)
    };

    put = (url, options) => {
        const fetchData = {
            method: 'PUT',
            body: options,
            headers: this.headers
        };

        return this.fetchCall(url, fetchData);
    };

    delete = (url, options) => {
        const fetchData = {
            method: 'DELETE',
            body: options,
            headers: this.headers
        };

        return this.fetchCall(url, fetchData)
    };

    fetchCall = (url, data) => {
        return fetch(url, data)
            .then(response => {
                const formattedResponse = response.map(this.transformResponse)
                formattedResponse.json()
            })
            .catch((error) => this.handleError(error))
            .then(response => response);
    };


    transformResponse(responseBlob){
        // additional response mapping will be done here before sending back the mapped response
        return responseBlob;
    }

    handleError = (error) => {
        let response = error; // new ApiResponseErrorModel(error);

        if (response.type === ErrorTypes.Redirect && response.redirectTo) {
            // window.location.href = response.redirectTo;
            // return Observable.throw(response);
        }
        else if (response.type === ErrorTypes.ForbiddenPage) {
            // this.router.navigate([RoutePath.Forbidden]);
        }
        else if (response.type === ErrorTypes.ErrorPage) {
            // this.router.navigate([RoutePath.Failed]);
        }

        // return Observable.throw(response);

    }

    errorHandler = (error) => {
        console.error('There has been a problem with your Api operation: ', error.message);
    };

}

const apiUtil = new ApiUtil();
Object.freeze(apiUtil);

export default apiUtil;
