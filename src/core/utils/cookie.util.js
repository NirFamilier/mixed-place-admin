// @flow

export class CookiesUtil {

    public static getCookie(name) {
        const ca = document.cookie.split(';');
        const caLen = ca.length;
        const cookieName = name + "=";
        let c;

        for (let i = 0; i < caLen; i += 1) {
            c = ca[i].replace(/^\s\+/g, "").trim();
            if (c.indexOf(cookieName) === 0) {
                return c.substring(cookieName.length, c.length);
            }
        }
        return '';
    }

    public static deleteCookie(name) {
        const expirationDate = new Date();
        expirationDate.setSeconds(expirationDate.getMinutes() - 1);

        document.cookie = `${name}=; expires=${expirationDate.toUTCString()};path=/`;
    }

    public static setCookieExplicitExp(name, value, expires, path = "") {
        let d = new Date(expires);
        document.cookie = `${name}=${value}; expires=${d.toUTCString()};path=/`;
    }

    public static setCookie(name, value, expireSec, path = "") {
        const d = new Date();
        d.setTime(d.getTime() + (expireSec  * 1000));
        const expires = d.toUTCString();
        document.cookie = `${name}=${value}; expires=${expires};path=/`;
    }

}

const cookiesUtil = new CookiesUtil();
Object.freeze(cookiesUtil);

export default cookiesUtil;