// @flow

export const ErrorTypes = {
        Redirect: 'Redirect',
        ForbiddenPage: 'ForbiddenPage',
        ErrorPage: 'ErrorPage',
}