// @flow

export const ModelsMetadata = {
    modelId: {title: 'Model ID', step: '1', isSortable: true, dataType: 'string'},
    modelName: {title: 'Model name', step: '2', isSortable: true, dataType: 'string'},
    modelType: {title: 'Model category', step: '3', isSortable: true, dataType: 'string'},
    modelStatus: {title: 'Model status', step: '4', isSortable: true, dataType: 'string'},
    dateEntered: {title: 'Date entered', step: '5', isSortable: true, dataType: 'string'},
    location: {title: 'Location', step: '6', isSortable: true, dataType: 'string'},
    modelSize: {title: 'Model size', step: '7', isSortable: true, dataType: 'string'},
    priceType: {title: 'Price type', step: '8', isSortable: true, dataType: 'string'},
    price: {title: 'Price', step: '9', isSortable: true, dataType: 'string'},
    unitPurchased: {title: 'Number of units purchased', step: '10', isSortable: true, dataType: 'string'},
    actions: {title: 'Actions', step: '11', isSortable: true, dataType: 'string'}
};
