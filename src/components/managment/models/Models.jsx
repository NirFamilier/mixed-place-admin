// @flow

import React, { Component } from 'react';
import './Models.css';
import TableComponent from '../../shared/Table/TableComponent';
import {ModelsMetadata} from "./models/metadata";

class Models extends Component {
    data=['123', 'something', 'something', 'Active', '31/01/18', 'Israel', 'Big', '$', '111', '23', '...'];

    render() {
        return(
            <div>
                <h4>Models Management</h4>

                <div className="add-new-wrapper">
                    <button type="button" className="pt-button pt-minimal pt-icon-add">New Model</button>
                </div>
                <TableComponent metadata={ModelsMetadata} data={this.data}/>

            </div>
        )
    }
}

export default Models
