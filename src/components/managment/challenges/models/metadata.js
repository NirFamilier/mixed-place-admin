// @flow

export const ChallengesMetadata = {
    id: {title: 'Callenge ID', step: '1', isSortable: true, dataType: 'string'},
    name: {title: 'Callenge name', step: '2', isSortable: true, dataType: 'string'},
    challengeTypeName: {title: 'Callenge type', step: '3', isSortable: true, dataType: 'string'},
    challengeStatusName: {title: 'Status', step: '4', isSortable: true, dataType: 'string'},
    startDate: {title: 'Start date', step: '5', isSortable: true, dataType: 'string'},
    endDate: {title: 'End date', step: '6', isSortable: true, dataType: 'string'},
    reward: {title: 'Reward Amount', step: '7', isSortable: true, dataType: 'string'},
    unlimitedDuration: {title: 'Unlimited Duration', step: '8', isSortable: true, dataType: 'string'},
    completedUsersCount: {title: 'Num of users Completed', step: '9', isSortable: true, dataType: 'string'},
    actions: {title: 'Actions', step: '10', isSortable: true, dataType: 'string'}
};