// @flow

import React, { Component } from 'react';
import './Challenges.css';
import {ChallengesMetadata} from "./models/metadata";

class Challenges extends Component {
    data = [
        {
            id: 1,
            name: "NewChallenge 1",
            creationDate: "0001-01-01T00:00:00",
            startDate: null,
            endDate: null,
            reward: 123,
            unlimitedDuration: true,
            description: "Description",
            challengeStatusId: 1,
            challengeStatusName: null,
            challengeTypeId: 1,
            challengeTypeName: null,
            completedUsersCount: 0,
            logicalOperator: 0,
            rules: [
                {
                    id: 1,
                    createDate: "0001-01-01T00:00:00",
                    amount: 100,
                    actionTypeId: 1,
                    condition: 0
                }
            ]
        },
        {
            id: 2,
            name: "NewChallenge 2",
            creationDate: "0001-01-01T00:00:00",
            startDate: null,
            endDate: null,
            reward: 123,
            unlimitedDuration: true,
            description: "Description",
            challengeStatusId: 1,
            challengeStatusName: null,
            challengeTypeId: 1,
            challengeTypeName: null,
            completedUsersCount: 0,
            logicalOperator: 0,
            rules: [
                {
                    id: 1,
                    createDate: "0001-01-01T00:00:00",
                    amount: 100,
                    actionTypeId: 1,
                    condition: 0
                }
            ]
        },
        {
            id: 3,
            name: "NewChallenge 3",
            creationDate: "0001-01-01T00:00:00",
            startDate: null,
            endDate: null,
            reward: 123,
            unlimitedDuration: true,
            description: "Description",
            challengeStatusId: 1,
            challengeStatusName: null,
            challengeTypeId: 1,
            challengeTypeName: null,
            completedUsersCount: 0,
            logicalOperator: 0,
            rules: [
                {
                    id: 1,
                    createDate: "0001-01-01T00:00:00",
                    amount: 100,
                    actionTypeId: 1,
                    condition: 0
                }
            ]
        }
    ];

    render() {
        return(
            <div>
                <h4>Challenges Management</h4>
                {/*<TableComponent metadata={ChallengesMetadata} data={this.data}/>*/}
            </div>
        )
    }
}

export default Challenges
