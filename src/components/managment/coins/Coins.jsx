// @flow

import React, { Component } from 'react';
import './Coins.css';

class Coins extends Component {
    metadata = {
        ruleId: {title: 'Rule Id', step: '1', isSortable: true, dataType: 'string'},
        ruleName: {title: 'Rule name', step: '2', isSortable: true, dataType: 'string'},
        type: {title: 'Type', step: '3', isSortable: true, dataType: 'string'},
        dateEntered: {title: 'Date entered', step: '4', isSortable: true, dataType: 'email'},
        userAffected: {title: 'User affected', step: '5', isSortable: true, dataType: 'string'},
        action: {title: 'Actions', step: '6', isSortable: true, dataType: 'string'}
    };

    render() {
        return(
            <div>
                <h4>Coins Management</h4>
            </div>
        )
    }
}

export default Coins
