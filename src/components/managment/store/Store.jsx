// @flow

import React, { Component } from 'react';
import './Store.css';
import {StoreMetadata} from "./models/metadata";
import TableComponent from '../../shared/Table/TableComponent';
import DialogPopupComponent from "../../shared/DialogPopup/DialogPopupComponent";

class Store extends Component {
    constructor(props) {
        super(props);

        this.data = ['123', 'something', 'Happy', 'Good', '31/01/18', '23', '$', '20', '20 inch', '50', '...'];
        this.dialogMetadata = {test: 'metadata 123'};
        this.dialogData = {test: 'data 123'};
        this.state = {
            dialogOptions: {icon: 'shop', title: 'Add New Store', isOpen: false, onClose: this.toggleDialog}
        };
    }


    toggleDialog = () => {
        this.setState({
            dialogOptions: {...this.state.dialogOptions, isOpen: !this.state.dialogOptions.isOpen},

        });
    };

    render() {
        return(
            <div>
                <h4>Store Management</h4>
                <div className="add-new-wrapper">
                    <button type="button" className="pt-button pt-minimal pt-icon-add" onClick={this.toggleDialog}>New Store</button>
                </div>
                <TableComponent metadata={StoreMetadata} data={this.data}/>
                <DialogPopupComponent  dialogOptions={this.state.dialogOptions} metadata={this.dialogMetadata} data={this.dialogData}/>
            </div>
        )
    }
}

export default Store
