// @flow

export const StoreMetadata = {
    packageId: {title: 'Package ID', step: '1', isSortable: true, dataType: 'string'},
    packageName: {title: 'Package name', step: '2', isSortable: true, dataType: 'string'},
    packageType: {title: 'Package category', step: '3', isSortable: true, dataType: 'string'},
    packageStatus: {title: 'Package status', step: '4', isSortable: true, dataType: 'string'},
    dateEntered: {title: 'Date entered', step: '5', isSortable: true, dataType: 'string'},
    listItems: {title: 'List of items', step: '6', isSortable: true, dataType: 'string'},
    priceType: {title: 'Price type', step: '7', isSortable: true, dataType: 'string'},
    price: {title: 'Price', step: '8', isSortable: true, dataType: 'string'},
    packageSize: {title: 'package size', step: '9', isSortable: true, dataType: 'string'},
    unitPurchased: {title: 'Number of units purchased', step: '10', isSortable: true, dataType: 'string'},
    actions: {title: 'Actions', step: '10', isSortable: true, dataType: 'string'}
};
