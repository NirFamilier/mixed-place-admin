// @flow

export const UserMetadata = {
    userId: {title: 'User id', step: '1', isSortable: true, dataType: 'string'},
    userName: {title: 'User name', step: '2', isSortable: true, dataType: 'string'},
    firstName: {title: 'First name', step: '3', isSortable: true, dataType: 'string'},
    lastName: {title: 'Last name', step: '4', isSortable: true, dataType: 'string'},
    age: {title: 'Age', step: '5', isSortable: true, dataType: 'string'},
    phoneNumber: {title: 'Mobile', step: '6', isSortable: true, dataType: 'string'},
    registrationDate: {title: 'Date registered', step: '7', isSortable: true, dataType: 'date'},
    userStatusName: {title: 'User status', step: '8', isSortable: true, dataType: 'string'},
    coinBalance: {title: 'Coin balance', step: '9', isSortable: true, dataType: 'string'},
    mpLevel: {title: 'Mixed places level', step: '10', isSortable: true, dataType: 'string'},
    mpCount: {title: 'Num of MPs', step: '11', isSortable: true, dataType: 'number'},
    designLevel: {title: 'Design Level', step: '12', isSortable: true, dataType: 'string'},
    challengesLevel: {title: 'Challenges Level', step: '13', isSortable: true, dataType: 'string'},
    socialLevel: {title: 'Social level', step: '14', isSortable: true, dataType: 'string'},
    isReported: {title: 'Is reported', step: '15', isSortable: true, dataType: 'boolean'},
    reportsCount: {title: 'Num of reports', step: '16', isSortable: true, dataType: 'number'}
};

export const MixedPlaceMetadata = {
    mpId: {title: 'MP id', step: '1', isSortable: true, dataType: 'string'},
    mpName: {title: 'MP name', step: '2', isSortable: true, dataType: 'string'},
    mpStatus: {title: 'MP Status', step: '3', isSortable: true, dataType: 'string'},
    designStatus: {title: 'Design Status', step: '4', isSortable: true, dataType: 'string'},
    userId: {title: 'User ID', step: '5', isSortable: true, dataType: 'string'},
    purchasedTime: {title: 'Purchased Timestamp', step: '6', isSortable: true, dataType: 'string'},
    lastUpdate: {title: 'Last Update', step: '7', isSortable: true, dataType: 'string'},
    price: {title: 'Price', step: '8', isSortable: true, dataType: 'string'},
    location: {title: 'Location', step: '9', isSortable: true, dataType: 'string'},
    visits: {title: 'Visits', step: '10', isSortable: true, dataType: 'string'},
    likes: {title: 'Likes', step: '11', isSortable: true, dataType: 'string'},
    comments: {title: 'Comments', step: '12', isSortable: true, dataType: 'string'},
    shares: {title: 'Shares', step: '13', isSortable: true, dataType: 'string'},
    demand: {title: 'Demand', step: '14', isSortable: true, dataType: 'string'},
    reported: {title: 'Reported', step: '15', isSortable: true, dataType: 'string'},
    tags: {title: 'Tags', step: '16', isSortable: true, dataType: 'string'},
    actions: {title: 'Actions', step: '17', isSortable: true, dataType: 'string'}
};