// @flow

import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import './Users.css';
import {UserMetadata} from "./models/metadata";
import ApiUtil from "../../../core/utils/api.utils";
import TableComponent from '../../shared/Table/TableComponent';
import PopupMenuComponent from '../../shared/PopupMenu/PopupMenuComponent';

class Users extends Component {
    data = [
        {
            userId: "0531f1f2-6ec8-4bc1-b731-3283befb2415", // V
            userName: "v.sergey5@msbitsoftware.com", // V
            registrationDate: "0001-01-01T00:00:00", // V
            coinBalance: 0, // V
            userStatusId: 0,
            userStatusName: null, // V
            mpLevel: 0, // V
            mpCount: 0, // V
            designLevel: 0, // V
            socialLevel: 0, // V
            challengeLevel: 0, // V
            reportsCount: 0, // V
            isReported: false, // V
            email: "v.sergey5@msbitsoftware.com",
            firstName: "Donatelo", // V
            lastName: "Avi", // V
            street: "Street",
            streetNumber: 1,
            city: "City",
            country: "Country",
            gender: true,
            age: 12, // V
            userDescription: "Description",
            phoneNumber: null, // V
            image: "MyPhoto"
        },
        {
            userId: "0531f1f2-6ec8-4bc1-b731-3283befb2415",
            userName: "v.sergey5@msbitsoftware.com",
            registrationDate: "0001-01-01T00:00:00",
            coinBalance: 0,
            userStatusId: 0,
            userStatusName: null,
            mpLevel: 0,
            mpCount: 0,
            designLevel: 0,
            socialLevel: 0,
            challengeLevel: 0,
            reportsCount: 0,
            isReported: false,
            email: "v.sergey5@msbitsoftware.com",
            firstName: "Donatelo",
            lastName: "Avi",
            street: "Street",
            streetNumber: 1,
            city: "City",
            country: "Country",
            gender: true,
            age: 12,
            userDescription: "Description",
            phoneNumber: null,
            image: "MyPhoto"
        },
        {
            userId: "0531f1f2-6ec8-4bc1-b731-3283befb2415",
            userName: "v.sergey5@msbitsoftware.com",
            registrationDate: "0001-01-01T00:00:00",
            coinBalance: 0,
            userStatusId: 0,
            userStatusName: null,
            mpLevel: 0,
            mpCount: 0,
            designLevel: 0,
            socialLevel: 0,
            challengeLevel: 0,
            reportsCount: 0,
            isReported: false,
            email: "v.sergey5@msbitsoftware.com",
            firstName: "Donatelo",
            lastName: "Avi",
            street: "Street",
            streetNumber: 1,
            city: "City",
            country: "Country",
            gender: true,
            age: 12,
            userDescription: "Description",
            phoneNumber: null,
            image: "MyPhoto"
        }
    ];





    render() {

        return(
            <div>
                <h4>Users Management</h4>
                <TableComponent metadata={UserMetadata} data={this.data}/>

            </div>
        )
    }
}

export default Users
