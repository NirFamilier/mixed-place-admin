// @flow

import React, {Component} from 'react';
import {
    // BrowserRouter as Router,
    // Route,
    Link
} from 'react-router-dom'
import './Sidebar.css';
import {SidebarMetadata} from "./models/metadata";

class Sidebar extends Component {

    render() {
        let item = Object.keys(SidebarMetadata).map((category) =>
            Object.keys(SidebarMetadata[category].items).map((item) =>
                <Link to={SidebarMetadata[category].items[item].link}>
                    <span className={'pt-menu-item ' + SidebarMetadata[category].items[item].icon}>
                        {SidebarMetadata[category].items[item].title}
                    </span>
                </Link>
            )
        );


        let cat = Object.keys(SidebarMetadata).map((category, i) => {
            let currentCat = SidebarMetadata[category];
            return (
            <ul key={`pt-menu-${category}`} className="pt-menu">
                <li key={currentCat.title} className='pt-menu-header'>
                    <h6>
                        {currentCat.title}
                    </h6>
                </li>
                <li>
                    {Object.keys(currentCat.items).map((item) =>
                    <Link key={currentCat.items[item].title} to={currentCat.items[item].link}>
                        <span className={'pt-menu-item ' + currentCat.items[item].icon} >
                            {currentCat.items[item].title}
                        </span>
                    </Link>
                    )}
                </li>
            </ul>
            )
        })



        return (

            <div className="sidebar pt-card">
                {cat}
            </div>
        )
    }
}

export default Sidebar
