export const SidebarMetadata = {
    quickAccess: {
        title: 'Quick Access',
        items: {
            users:      { title: 'Users',       icon: 'pt-icon-people', step: 1, link: '/users' },
            models:     { title: 'Models',      icon: 'pt-icon-globe', step: 2, link: '/models' },
            coins:      { title: 'Coins',       icon: 'pt-icon-dollar', step: 3, link: '/coins' },
            challenges: { title: 'Challenges',  icon: 'pt-icon-badge',  step: 4, link: '/challenges' },
        }

    },
    manage: {
        title: 'Manage',
        items: {
            users:      { title: 'Users',       icon: 'pt-icon-people', step: 1, link: '/users' },
            challenges: { title: 'Challenges',  icon: 'pt-icon-badge',  step: 2, link: '/challenges' },
            store:      { title: 'Store',       icon: 'pt-icon-shop',   step: 3, link:'/store' },
            coins:      { title: 'Coins',       icon: 'pt-icon-dollar', step: 4, link: '/coins' },
            models:     { title: 'Models',      icon: 'pt-icon-globe', step: 5, link: '/models' },
            analytics:  { title: 'Analytics',   icon: 'pt-icon-timeline-line-chart', step: 6, link: '/analytics' }
        }
    }

};
