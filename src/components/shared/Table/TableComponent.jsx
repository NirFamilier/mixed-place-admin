// @flow

import React, { Component } from 'react';
import './TableComponent.css';
import {FormatApiData, SortFormattedApiData} from "./table.utils";
import PopupMenuComponent from "../PopupMenu/PopupMenuComponent";

export default class TableComponent extends Component{
    metadata = {};
    data = {};
    formatedData = {};
    isMenuShown = null;

    constructor(props) {
        super(props);
        this.metadata = props.metadata;
        this.data = props.data;
        this.showMenu = this.showMenu.bind(this);
    }

    showMenu = (isMenuShown) => {
        console.log('menu toggle clicked');
        this.isMenuShown = isMenuShown;
    }

    someAction(){
        return true;
    }

    render() {
        this.formatedData = FormatApiData(this.data, this.metadata);

        return (
            <div>
                <table className="pt-table pt-condensed pt-striped pt-bordered">
                    <thead>
                        <tr>
                            {Object.keys(this.metadata).map((column, i) => {
                                return (
                                    <th key={`${column}-metadata-${i}`}>{this.metadata[column].title}</th>
                                )
                            })}
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {this.data.map((item, x) => {
                        return (
                            <tr key={x}>
                            {Object.keys(this.metadata).map((column, y) => {
                                return (
                                    <td key={`${column}-data-${y}`}>{item[column]}</td>
                                )
                            })}
                            <td>
                                <span className="pt-icon-more" onClick={this.showMenu(x)}></span>
                            { (this.isMenuShown == x)?<PopupMenuComponent data={{icon: '', value: '', id:''}} itemClick={this.someAction} />: '' }
                            </td>
                        </tr>
                        )
                    })}
                    </tbody>
                </table>
            </div>
            )
    }
}