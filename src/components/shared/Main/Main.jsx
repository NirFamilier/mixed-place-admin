// @flow

import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Route,
    // Link
} from 'react-router-dom'

import Users from "../../managment/users/Users";
import Store from "../../managment/store/Store";
import Models from '../../managment/models/Models';
import Coins from '../../managment/coins/Coins';
import Challenges from '../../managment/challenges/Challenges';
import Analytics from '../../managment/analytics/Analytics';

import './Main.css';


class Main extends Component {

    render() {
        return(
            <Router>
                <div className="main">
                    <div id="container" className="content pt-card">
                        <Route path="/users" component={Users}/>
                        <Route path="/store" component={Store}/>
                        <Route path="/models" component={Models}/>
                        <Route path="/coins" component={Coins}/>
                        <Route path="/challenges" component={Challenges}/>
                        <Route path="/analytics" component={Analytics}/>
                    </div>
                </div>
            </Router>
        )
    }
}

export default Main
