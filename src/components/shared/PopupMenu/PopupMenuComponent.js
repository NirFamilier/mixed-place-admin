import React, { Component } from 'react';
import './PopupMenuComponent.css';
import { Menu, MenuItem, MenuDivider } from "@blueprintjs/core";

export default class PopupMenuComponent extends Component {

    constructor(props){
        super(props);
        this.data = props.data;
        this.itemClick = props.itemClick;
    }

    /***
     * set css top and left positions for the menu according to the parent element position
     * @param pos - objet parameter with top and left of the parent element
     */
    placePopupMenu(pos) {
        const popupContainer = document.querySelector('.menu-container');
        popupContainer.setAttribute('style', 'display: block; top:' + ((pos.top / 10) + 2) + 'rem; left: ' + ((pos.left / 10) - 5) + 'rem');
    }


    /***
     * return the offset for the top and left position of the parent element
     * @param element - parent element
     */
    getParentElementPagePosition(element) {
        const bodyPos = document.body.getBoundingClientRect();
        const elPos = element.getBoundingClientRect();
        const offsetY = elPos.top - bodyPos.top;
        const offsetX = elPos.left - bodyPos.left;
        return { top: offsetY, left: offsetX };
    }

    /***
     * close the popup menu by changing the css display from block to none
     * @param showPopup
     * @returns {boolean}
     */
    closeMenu(showPopup) {
        const popupContainer = document.querySelector('.menu-wrapper');
        const popupAttr = popupContainer.getAttribute('style');
        if (popupAttr.indexOf('display: block') > -1) {
            popupContainer.setAttribute('style', 'display: none');
            return false;
        } else {
            return true;
        }
    }

    render() {
        return (
            <div className="menu-wrapper hidden">
                <Menu>
                    <MenuItem
                        iconName="new-text-box"
                        onClick={this.handleClick}
                        text="New text box"
                    />
                    <MenuItem
                        iconName="new-object"
                        onClick={this.handleClick}
                        text="New object"
                    />
                    <MenuItem
                        iconName="new-link"
                        onClick={this.handleClick}
                        text="New link"
                    />
                    <MenuDivider />
                    <MenuItem text="Settings..." iconName="cog" />
                </Menu>
            </div>
        );
    }

    handleClick(e) {
        console.log("clicked", (e.target).textContent);
    }
}