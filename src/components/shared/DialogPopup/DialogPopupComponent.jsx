// @flow

import React, { Component } from 'react';
import {Dialog, Button, Intent} from '@blueprintjs/core';
import './DialogPopupComponent.css';

export default class DialogPopupComponent extends Component{
    metadata = {};
    data = {};

    constructor(props) {
        super(props);
        this.metadata = props.metadata;
        this.data = props.data;
        this.dialogOptions = props.dialogOptions;
    }

    save = () => {
        console.log('saving data...');
        this.props.dialogOptions.onClose();
    };

    cancel = () => {
        console.log('canceling...');
        this.props.dialogOptions.onClose();
    };

    render() {

        return (
            <div>
                <Dialog
                    iconName={this.props.dialogOptions.icon}
                    isOpen={this.props.dialogOptions.isOpen}
                    onClose={this.props.dialogOptions.onClose}
                    title={this.props.dialogOptions.title}
                    autoFocus="true"
                    canEscapeKeyClose="true"
                    enforceFocus="true"
                    isCloseButtonShown="true"
                >
                    <div className="pt-dialog-body">
                        Some content
                    </div>
                    <div className="pt-dialog-footer">
                        <div className="pt-dialog-footer-actions">
                            <Button text="Cancel" onClick={this.cancel}/>
                            <Button
                                intent={Intent.PRIMARY}
                                onClick={this.save}
                                text="Save"
                            />
                        </div>
                    </div>
                </Dialog>
            </div>
        )
    }

}