// @flow

import React, { Component } from 'react';
// import Nav from '@blueprintjs/core';
import './Header.css';

class Header extends Component {

    render() {
        return(
            <div>
                <header className="header">
                    <nav className="pt-navbar pt-dark .modifier">
                        <div className="pt-navbar-group pt-align-left">
                            <div className="pt-navbar-heading">MixedPlace Admin</div>

                        </div>
                        <div className="pt-navbar-group pt-align-right">
                            <input className="pt-input" placeholder="Search files..." type="text" />
                            {/*<button class="pt-button pt-minimal pt-icon-home">Home</button>*/}
                            {/*<button class="pt-button pt-minimal pt-icon-document">Files</button>*/}
                            <span className="pt-navbar-divider"></span>
                            <button className="pt-button pt-minimal pt-icon-user">User</button>
                            {/*<button class="pt-button pt-minimal pt-icon-notifications"></button>*/}
                            {/*<button class="pt-button pt-minimal pt-icon-cog"></button>*/}
                        </div>
                    </nav>
                </header>

            </div>
        )
    }
}

export default Header
